﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System;

namespace InfBuddy
{
    public class MoveToMissionWallBugFixSpotState : IState
    {
        public IState GetNextState()
        {
            if (!InfBuddy.NavMeshMovementController.IsNavigating && IsAtQuestStarter())
                return InfBuddy.IsLeader ? (IState)new StartMissionState() : (IState)new DefendSpiritState();

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("MoveToMissionWallBugFixSpot::OnStateEnter");

            InfBuddy.NavMeshMovementController.SetNavMeshDestination(Constants.MissionWallBugFixSpot);
        }

        public void OnStateExit()
        {
            Chat.WriteLine("MoveToMissionWallBugFixSpot::OnStateExit");
        }

        public void Tick()
        {
        }

        private bool IsAtQuestStarter()
        {
            return DynelManager.LocalPlayer.Position.DistanceFrom(Constants.QuestStarterPos) < 4f;
        }
    }
}
